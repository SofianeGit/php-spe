<?php

function encoder($texte, $decalage = 14){
	$ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	$i = 0;
	$resultat="";
	while($i < strlen($texte)){
		$caractere = substr($texte, $i, 1);
		$position = strpos($ALPHABET, $caractere);
		$position = $position + $decalage;
		if($position >= strlen($ALPHABET)){
			$position = $position - strlen($ALPHABET);
		}
		$code = substr($ALPHABET, $position, 1);
		$resultat = $resultat.$code;
		$i = $i + 1;
	}
	return $resultat;
}

echo encoder('NMLA').'<br>';

function decoder($texte){
	return encoder($texte, -14);
}

echo decoder('BAZO');