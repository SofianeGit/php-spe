<?php


/**
* Class Form
* Permet de generer un formulaire rapidement et simplement
*/

class form{
	
	private $data; // variable de type array : données utilisées par le formulaire
	public $paragraphe = 'p'; // variable de type string : utilisé pour entourer les champs
	
	public function __construct($data = array()){
		$this->data = $data;//$_POST
	}
	
	private function paragraphe($html){
		return"<{$this->paragraphe}>{$html}</{$this->paragraphe}>";
	}
	
	/*
		Index de la valeur à récupérer
	*/
	
	private function getValue($index){
		return isset($this->data[$index]) ? $this->data[$index] : null;
	}

	
	public function input10($name, $tab){
		$html = '<p style="color:blue;">Participants :</p><select class="js-example-basic-multiple" style="color:blue;" name="'.$name.'[]"  multiple="multiple">';
		foreach($tab as $value){
			$html .= '<datalist>
						<option value="'.$value.'">'.$value.'</option>
						</datalist>';
		}
		$html .= '</select><br><br>';

		return  $this->paragraphe($html);
		
	}

}

$idParticipants = array();

	if((!isset($_POST['personnes']))){
		 echo ' ';
		}else{	
			$bdd = new PDO('mysql:host=localhost;dbname=cadeaux;charset=utf8', 'root', '');
			$stmt = $bdd->prepare("INSERT INTO participants (participants) VALUES (:participants)");
			
			foreach ($_POST["personnes"] as $personne) {			
				$stmt->bindParam(':participants', $personne);
				$stmt->execute();
				$idParticipants[] = $bdd->lastInsertId();
			}
		}
	
?>

	<script type="text/javascript">
				$(document).ready(function() {
		$('.js-example-basic-multiple').select2();
				});
				</script>
			</body>