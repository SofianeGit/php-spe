<?php


$host = '127.0.0.1';
$db   = 'cadeaux';
$user = 'root';
$pass = '';
$charset = 'utf8';

$options = [
   PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
   PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
   PDO::ATTR_EMULATE_PREPARES   => false,
];
$dsn = "mysql:host=$host;dbname=$db;charset=$charset";
try {
    $pdo = new PDO($dsn, $user, $pass, $options);
} catch (\PDOException $e) {
    throw new \PDOException($e->getMessage(), (int)$e->getCode());
}




?>
<html>
<p style="text-align:center; color:red; font-size:xx-large; text-decoration:underline;">
	<br><br>Application tirage au sort
</p>

<form action="tirAge.php" method="post">
<p style="color:blue;">
	Organisateur : <input type="text" name="createur" /><br><br>
	Date : <input type="date" name="date" /><br><br>
	<input type="submit" value="Valider" />
</P>
</form>
</html>

