<?php


/**
* Class Form
* Permet de generer un formulaire rapidement et simplement
*/

class form2{
	
	private $data; // variable de type array : données utilisées par le formulaire
	public $paragraphe = 'p'; // variable de type string : utilisé pour entourer les champs
	
	public function __construct($data = array()){
		$this->data = $data;//$_POST
	}
	
	private function paragraphe($html){
		return"<{$this->paragraphe}>{$html}</{$this->paragraphe}>";
	}
	
	/*
		Index de la valeur à récupérer
	*/
	
	private function getValue($index){
		return isset($this->data[$index]) ? $this->data[$index] : null;
	}
	
	
	public function input1($createur, $date, $nomTirage){
		return $this->paragraphe (
		'<label for="'.$nomTirage.'" style="color:blue;"> Nom du tirage :</label>
		<input type="text" name="'. $nomTirage. '"value="'.$this->getValue($nomTirage). '">
		<br><br>
		<label for="'.$createur.'" style="color:blue;"> Organisateur :</label>
		<input type="text" name="'. $createur. '"value="'.$this->getValue($createur). '">
		<br><br>
		<label for="'.$date.'" style="color:blue;">Date :</label>
		 <input type="date" style="color:blue;" name="'. $date. '"value="'.$this->getValue($date). '">'
		);
		
	}

}

if((!isset($_POST['date'])) || (!isset($_POST['createur'])) || (!isset($_POST['nomTirage']))){
		 echo ' ';
	}else{	
		$bdd = new PDO('mysql:host=localhost;dbname=cadeaux;charset=utf8', 'root', '');
		$stmt = $bdd->prepare("INSERT INTO tirage (nomCreateur, Date, nomTirage) VALUES (:nomCreateur, :Date, :nomTirage)");
		$stmt->bindParam(':nomCreateur', $_POST["createur"]);
		$stmt->bindParam(':Date', $_POST["date"]);
		$stmt->bindParam(':nomTirage', $_POST["nomTirage"]);
		$stmt->execute();
		$idTirage = $bdd->lastInsertId();
	}

?>