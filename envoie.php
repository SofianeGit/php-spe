<?php


/**
* Class Form
* Permet de generer un formulaire rapidement et simplement
*/

class Envoie{
	
	private $data; // variable de type array : données utilisées par le formulaire
	public $paragraphe = 'p'; // variable de type string : utilisé pour entourer les champs
	
	public function __construct($data = array()){
		$this->data = $data;//$_POST
	}
	
	private function paragraphe($html){
		return"<{$this->paragraphe}>{$html}</{$this->paragraphe}>";
	}
	
	public function submit(){
	return $this->paragraphe ('<button style="box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2), 0 6px 20px 0 rgba(0,0,0,0.19);   background-color: #008CBA;
								color: white; " type="submit" >Envoyer</button>');
	}
}