<?php
/*
class Personnage{
	
	public $vie = 80;
	public $atk = 20;
	public $nom;
	
	public function __construct($nom){
		$this->nom = $nom;
	}
	
	public function regenerer($vie){
		$this->vie = $vie;
	}
	
	public function mort(){
		return $this->vie == 0;
;	}
	
}
*/

/**
* Class Form
* Permet de generer un formulaire rapidement et simplement
*/

class formulaire{
	
	private $data; // variable de type array : données utilisées par le formulaire
	public $paragraphe = 'p'; // variable de type string : utilisé pour entourer les champs
	
	public function __construct($data = array()){
		$this->data = $data;//$_POST
	}
	
	private function paragraphe($html){
		return"<{$this->paragraphe}>{$html}</{$this->paragraphe}>";
	}
	
	/*
		Index de la valeur à récupérer
	*/
	
	private function getValue($index){
		return isset($this->data[$index]) ? $this->data[$index] : null;
	}
	
	
	public function input10($name,$mdp,$tel,$color,$date,$sexe,$diplome,$lang){
		return $this->paragraphe (
		'<label for="'.$name.'"> Votre pseudo :</label>
		<input type="text" name="'. $name. '"value="'.$this->getValue($name). '">
		<br><br>
		
		<label for="'.$mdp.'">Mot de passe :</label>
		<input type="password" name="'. $mdp. '"value="'.$this->getValue($mdp). '">
		<br><br>
		
		<label for="'.$tel.'">Tél :</label>
		<input type="number" name="'. $tel. '"value="'.$this->getValue($tel). '">
		<br><br>
		
		<label for="'.$color.'">Couleur favorite :</label>
		<input type="color" name="'. $color. '"value="'.$this->getValue($color). '">
		<br><br>
		
		<label for="'.$date.'">Date de naissance :</label>
		<input type="date" name="'. $date. '"value="'.$this->getValue($date). '">
		<br><br>
		
		<label for="'.$sexe.'">Un Homme : </label>
        <input type="radio" name="'.$sexe.'" value="'.$this->getValue($sexe). '" id="homme" >	
		<label for="'.$sexe.'">Une Femme : </label>
        <input type="radio" name="'.$sexe.'" value="'.$this->getValue($sexe). '" id="femme"><br><br>
		
		Quels langages maitrisez-vous ? <br/><br/>
		<label for="'.$lang.'">C : </label>
        <input type="checkbox" name="'.$lang.'.[]" value="'.$this->getValue($lang). '" id="C"><br>
			
        <label for="'.$lang.'">JAVA : </label>
        <input type="checkbox" name="'.$lang.'.[]" value="'.$this->getValue($lang). '" id="C"><br>
			
		<label for="'.$lang.'">PHP : </label>
        <input type="checkbox" name="'.$lang.'.[]" value="'.$this->getValue($lang). '" id="C"><br><br>
		
		 Dernier diplôme obtenue : <br><br>
         <select name="'.$diplome.'" class="form-inline">
			<optgroup label="Licence">
				<option value="Licence/economie gestion">Economie Gestion</option>
				<option value="Licence/maths">Maths</option>
				<option value="Licence/informatique">Informatique</option>
			</optgroup>
				
			<optgroup label="Master">
			
				<option value="Master/physique">MMM</option>
				<option value="Master/finance">Finance</option>
			</optgroup>
		</select><br><br>'
	
 
		);
	}

	
	public function submit(){
		return $this->paragraphe ('<button style="box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2), 0 6px 20px 0 rgba(0,0,0,0.19);   background-color: #008CBA;
									color: white; " type="submit" >Envoyer</button>');
	}
	
}

?>