<?php
$host = '127.0.0.1';
$db   = 'cadeaux';
$user = 'root';
$pass = '';
$charset = 'utf8';

$options = [
   PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
   PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
   PDO::ATTR_EMULATE_PREPARES   => false,
];
$dsn = "mysql:host=$host;dbname=$db;charset=$charset";
try {
    $pdo = new PDO($dsn, $user, $pass, $options);
} catch (\PDOException $e) {
    throw new \PDOException($e->getMessage(), (int)$e->getCode());
}

$data = [
   'participants' => "Mitra"
];

$data1 = [
   'participants' => "Ronan"
];

$data2 = [
   'participants' => "Mathieu"
];

$data3 = [
   'participants' => "Sofiane"
];

$data4 = [
   'participants' => "Benjamin"
];

$array = [$data,$data1,$data2,$data3,$data4];

foreach($array as $arr){
	$sql = "INSERT INTO participants (participants) VALUES (:participants)";
	$stmt= $pdo->prepare($sql);
	$stmt->execute($arr);
}

?>